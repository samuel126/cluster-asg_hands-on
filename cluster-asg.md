# Launching cluster autoscaling and horizontal pod autoscaling on a K3s cluster 

The purpose of this documentation is to explain creating a kubernetes cluster that can scale up and scale down via horizontal pod auto scaling and cluster auto scaling

* The main purpose of Cluster Autoscaler is to get pending pods a place to run. Cluster Autoscaler periodically checks whether there are any pending pods 

* Cluster Autoscaler increases the size of the cluster when:

    there are pods that failed to schedule on any of the current nodes due to insufficient resources.
    adding a node similar to the nodes currently present in the cluster would help.

* Cluster Autoscaler decreases the size of the cluster when:
     some nodes are consistently unneeded for a significant amount of time. 
     A node is unneeded when it has low utilization and all of its important pods can be moved elsewhere.

# Horizontal Pod Autoscaler with Cluster Autoscaler
Horizontal Pod Autoscaler changes the deployment's or replicaset's number of replicas based on the current CPU load. 

If the load increases, HPA will create new replicas, for which there may or may not be enough space in the cluster. 

If there are not enough resources, CA will try to bring up some nodes, so that the HPA-created pods have a place to run. 

If the load decreases, HPA will stop some of the replicas. As a result, some nodes may become underutilized or completely empty, and then CA will terminate such unneeded nodes.

# Cluster Autoscaler is different from CPU-usage-based node autoscalers
Cluster Autoscaler makes sure that all pods in the cluster have a place to run, no matter if there is any CPU load or not. Moreover, it tries to ensure that there are no unneeded nodes in the cluster.

CPU-usage-based (or any metric-based) cluster/node group autoscalers don't care about pods when scaling up and down. As a result, they may add a node that will not have any pods, or remove a node that has some system-critical pods on it, like kube-dns. Usage of these autoscalers with Kubernetes is discouraged.

# ProvideID : 
Cluster-asg looks for providerID to execute auto scaling at the cloud.

For AWS providerID is : aws:///<availability zone>/<instanceID>
But in k3s, providerID is: k3s: instance-ip

You can get zone name and instanceID with those commands:
zone name:
curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone

instanceID:
curl -s http://169.254.169.254/latest/meta-data/instance-id)


# Prerequisites

1. A k3s cluster ready and running on AWS EC2 Instances
* minimum node configuration with alpine linux:
   > Server node :  t3a.small
   > Agent node  :  t3a.nano

* server node user data for k3s installation with provideID:
curl -sfL https://get.k3s.io | sh -s - server \
  --token ${token} \
  --kubelet-arg=provider-id="aws:///$(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone)/$(curl -s http://169.254.169.254/latest/meta-data/instance-id)"


* agent node user data for k3s installation with provideID:
curl -sfL https://get.k3s.io | sh -s - agent \
  --server https://${host}:6443 \
  --token ${token} \
  --kubelet-arg=provider-id="aws:///$(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone)/$(curl -s http://169.254.169.254/latest/meta-data/instance-id)"

2. EC2 autoscaling group for k3s cluster
    > aws_autoscaling_group name = "projecx-asg-worker"
    > capacity_rebalance  = true #Capacity Rebalancing helps you maintain workload availability by proactively augmenting your fleet with a new Spot Instance before a running instance is interrupted by EC2.
    > desired_capacity    = 1
    > max_size            = 10
    > min_size            = 1
    > health_check_grace_period = 300
    > health_check_type         = "EC2"

    > mixed_instances_policy :
      > on_demand_base_capacity                  = 0
      > on_demand_percentage_above_base_capacity = 10
      > spot_allocation_strategy                 = "capacity-optimized" # automatically launches Spot Instances into the most available pools by looking at real-time capacity data and predicting which are the most available.

    > aws_autoscaling_policy:
      > name = "agents-scale-up"
      > scaling_adjustment = 1
      > adjustment_type = "ChangeInCapacity"
      > cooldown = 120

    > aws_autoscaling_policy:
      > name = "agents-scale-down"
      > scaling_adjustment = -1
      > adjustment_type = "ChangeInCapacity"
      > cooldown = 120
    
4. Prometheus should be installed:
   > sudo apk add prometheus (for alpine linux)

   

# Creating HPA

1. Create the components.yaml file and apply it to the k3s cluster:

sudo kubectl apply -f components.yaml

2. Create hpa.yaml for hpa deployment and apply :

sudo kubectl apply -f hpa.yaml


# Creating Cluster Autoscaler

1. Create IAM Roles for both Server Node and Worker Node:

a. Server Node IAM Role:
Create policies:
    > K3sServerNodePolicy
    > K3sAutoscalerPolicy
Add Tags:
    > k8s.io/cluster-autoscaler/enabled - <value>    
    > k8s.io/cluster-autoscaler/cluster - <value>
Name IAM role as K3s-Autoscaler-ServerNode and add this role to your running server node:
    > aws console>> ec2 >> Actions >> Security >> Modify IAM

b. Agent Node IAM Role:
Create policies:
    >  K3sAgentNodePolicy 
Add Tags:
    Add Tags:
    > k8s.io/cluster-autoscaler/enabled - <value>    
    > k8s.io/cluster-autoscaler/cluster - <value>
Name IAM role as K3s-Autoscaler-AgentNode and add this role to your running agent node:
    > aws console>> ec2 >> Actions >> Security >> Modify IAM 

c. Create cluster-asg.yaml for cluster autoscaling and apply
$ sudo kubectl apply -f cluster-asg.yaml





# Documents:
https://github.com/kubernetes/autoscaler/blob/master/cluster-autoscaler/FAQ.md#internals

https://rancher.com/docs/rancher/v2.0-v2.4/en/cluster-admin/cluster-autoscaler/amazon/

https://docs.google.com/document/d/17d4qinC_HnIwrK0GHnRlD1FKkTNdN__VO4TH9-EzbIY/edit

https://github.com/kubernetes/autoscaler/blob/21e9da59362ff4dea77930ecbe505057a1306827/cluster-autoscaler/cloudprovider/aws/examples/cluster-autoscaler-one-asg.yaml#L139

https://github.com/garutilorenzo/k3s-aws-terraform-cluster


